const http = require('http');
const port = 3080;
const fs = require('fs');
const path = require('path');
const semverRegex = require('semver-regex');
const pathFs = 'package.json';

const server = http.createServer((req, res) => {
    res.writeHead(200, { "Content-Type" : "text/plain" });
    res.end("server on port"+" "+port);
});

function ChangeVersion(version, versionDefault) {
    fs.readFile(pathFs, 'utf8', function(error, data){
        if (error){
        console.log(error);
        } else {
        data;
        };
    
    const json = JSON.parse(data);
    const jsonString = JSON.stringify(data);
    const packageCopy = "{\n  \"name\": \"changeversion\",\n  \"version\": \""+versionDefault+"\",\n  \"description\": \"\",\n  \"main\": \"app.js\",\n  \"scripts\": {\n    \"start\": \"node app.js\",\n    \"test\": \"echo \\\"Error: no test specified\\\" && exit 1\"\n  },\n  \"author\": \"\",\n  \"license\": \"ISC\",\n  \"dependencies\": {\n    \"node\": \"^14.5.0\",\n    \"semver-diff\": \"^3.1.1\",\n    \"semver-regex\": \"^3.1.1\"\n  }\n}\n";
    const semVer = semverRegex().test(version);
    if(semVer == true){
        console.log("La version du package est:" + " " + semVer + ", " + "la version "+ " " + json.version + " " + "est correct.");
    } else {
        fs.writeFile(pathFs, packageCopy, function(error){
            if (error)
            throw error;
        });
        console.error(
        "La version du package est:" + " " + semVer + ", " + "la version" + " " + version + " " + "est incorrect.");   
    }
  });
}

ChangeVersion("1.2", "1.0.0");

server.listen(port, () => {
    console.log("server on port"+" "+port+"!");
});

